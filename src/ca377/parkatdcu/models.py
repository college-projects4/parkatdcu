from django.db import models

# Create your models here.
class Campus(models.Model):
   campus_id = models.IntegerField(primary_key=True)
   name = models.CharField(max_length=100)
   open_time = models.IntegerField(default=7)
   close_time = models.IntegerField(default=21)
   location = models.URLField(max_length=1000, default="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8004.010781876666!2d-6.262383902137647!3d53.386087299134914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48670e1b2f9d5d03%3A0x12bb45984107c2a8!2sDublin%20City%20University!5e0!3m2!1sen!2sie!4v1638359746906!5m2!1sen!2sie")

   def __str__(self):
      return self.name

class Carpark(models.Model):
   carpark_id = models.IntegerField(primary_key=True)
   name = models.CharField(max_length=100)
   campus_id = models.ForeignKey(Campus,on_delete = models.CASCADE)
   spaces = models.IntegerField()
   disabled_spaces = models.IntegerField()
   paid_parking = models.CharField(max_length=10, default="Yes")

   def __str__(self):
      return self.name
