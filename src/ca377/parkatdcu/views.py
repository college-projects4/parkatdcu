from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from . models import Campus, Carpark
import requests, datetime
import json

# Create your views here.
def index(request):
    context = {
        "campuses" : Campus.objects.all()
    }
    return render(request,"parkatdcu/index.html",context)

def carparks(request):
    context = {}
    webservice_base_url = "http://mbezbradica.pythonanywhere.com/carparks/"

    # Sets campus_name to selected campus
    campus_name = request.GET['campus']

    try:
        campus = Campus.objects.get(name__iexact=campus_name)
    except Campus.DoesNotExist:
        return HttpResponse('<h1>No such campus</h1>')

    # Checks if campus is open based on opening and closing times in database compared to now
    campus_open = False

    weekday = datetime.datetime.today().weekday()
    hour = datetime.datetime.today().hour

    # Also checks if weekday
    if  weekday < 5 and hour > campus.open_time and hour < campus.close_time:
        campus_open = True

    carparks = Carpark.objects.filter(campus_id=campus)

    # Sets up info for each carpark in selected campus
    carpark_info = []
    for carpark in carparks:

        webservice_url = webservice_base_url + carpark.name

        realtime_info = requests.get(webservice_url).json()

        # This variable is used for colour coding the available spaces part of the table in carparks.html
        availability_status = ""

        if 'spaces_available' in realtime_info:
            spaces_available = realtime_info['spaces_available']
            if (spaces_available/carpark.spaces)*100 <= 10:
                availability_status = "table-danger"
            elif (spaces_available/carpark.spaces)*100 <= 20:
                availability_status = "table-warning"
            else:
                availability_status = "table-success"
        else:
            spaces_available = 'not available'

        carpark_info.append({
                             'name': carpark.name,
                             'spaces': carpark.spaces,
                             'disabled_spaces': carpark.disabled_spaces,
                             'paid_parking': carpark.paid_parking,
                             'spaces_available': spaces_available,
                             'availability_status' : availability_status,
                             }
                            )

    context['campus'] = campus
    context['campus_name'] = campus_name
    context['campuses'] = Campus.objects.all()
    context['campus_map'] = campus.location
    context['campus_open'] = campus_open
    context['carparks'] = carpark_info

    return render(request,"parkatdcu/carparks.html",context)
