# 2022-ca377-master-parkatdcu

CA377_2021_22 project 'Park at DCU'

# ParkAtDCU
This app allows the user to view the different carparks at different DCU campuses. The app will give them information such as the available spaces for each carpark,
the number of spaces for disabled people, and if the carpark is paid parking or not. The user will also be presented with an embedded Google map of where each campus is
so they can easily see where it is in relation to the other campuses.

### Installation for use on DCU Docker Container

1. Clone this repository with HTTPS

2. In your container, run: git clone *repository link*

3. Change directory to the first ca377 folder in the src folder

4. Run the following commands: 
python3 manage.py makemigrations
python3 manage.py migrate

5. Finally run the server with: python3 manage.py runserver 0.0.0.0:8080

This can be viewed at: https://containers.computing.dcu.ie/*username*/parkatdcu/

### Running Tests

In order to run the tests for this site, you first need to comment out lines 33 and 34 in settings.py.
Then, use the following command in the terminal to run tests:
python3 manage.py test parkatdcu
